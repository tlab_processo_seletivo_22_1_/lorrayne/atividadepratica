<h1>Processo Seletivo TerraLAB 22.1 - Frontend Web<h1>
<blockquote>
    <p>Atividade prática sobre Git e GitLab</p>
</blockquote>

<h3>Questão 3</h3>
<p>- O código é a representação de uma calculadora, a mesma recebe a entrada de dois argumentos pelo terminal, esses são transformados em inteiros e realizado a respectiva soma, tendo o resultado apresentado no terminal pelo console.log
<br>- Para executar no terminal é necessário ter o node instalado no computador e realizar o seguinte comando: node 'nome do arquivo' 'primeiro argumento' 'segundo argumento'
<br>-> ex: node calculadora.js 10 7
</p>

<h3>Questão 4</h3>
<p>- Git add <nome do arquivo> <br>
- O README.md deve ter “primeira submissão readme.md” <br>
- A calculadora deve ter “primeira submissão calculadora.js” <br>
</p>

<h3>Questão 5</h3>
<p>- O conventional commit será agora como refactor </p>

<h3>Questão 6</h3>
<p> - O código conta com dois erros, na soma e subtração a posição args[0] encontra-se errada, sendo o correto args[1] e args[2]<br>
- No default o argumento arg[0] também está errado, sendo o correto args[0] com o ‘s’ no fim. <br>
- comando: node 'nome do arquivo' 'sub ou soma' 'primeiro inteiro' 'segundo inteiro'<br>
- ex: node calculadora.js sub 5 4
</p>

<h3>Questão 10</h3>
<p>- Antes de executar adicionei ao código a linha: <br>
-> const args = process.argv.slice(2); <br>
- Para execução compilo o comando: node calculadora.js 'numero' ('/' ou '+' ou '-') 'numero' <br>
- A função Eval avalia o expressão de cadeia de caracteres e retorna seu valor.
</p>